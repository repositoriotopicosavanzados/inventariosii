/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Inventarios;

/**
 *
 * @author Yuzel
 */
public class Almacen {
    private int clave;
    private String nombre;

    public Almacen() {
    }

    public Almacen(int clave) {
        this.clave = clave;
    }

    public Almacen(int clave, String nombre) {
        this.clave = clave;
        this.nombre = nombre;
    }

    public int getClave() {
        return clave;
    }

    public void setClave(int clave) {
        this.clave = clave;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public String toString() {
        return "Almacen{" + "clave=" + clave + ", nombre=" + nombre + '}';
    }

    public void imprimir(){
        System.out.println(toString());
    }
}

