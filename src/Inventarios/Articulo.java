package Inventarios;

public class Articulo {
    private int clave;
    private String nombre;

    public Articulo() {
    }

    public Articulo(int clave) {
        this.clave = clave;
    }

    public Articulo(int clave, String nombre) {
        this.clave = clave;
        this.nombre = nombre;
    }

    public int getClave() {
        return clave;
    }

    public void setClave(int clave) {
        this.clave = clave;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public String toString() {
        return "Articulo{" + "clave=" + clave + ", nombre=" + nombre + '}';
    }

    public void imprimir(){
        System.out.println(toString());
    }


}

