
package inv;

public  class existencia {
    
    private int exist;
    private almacen almacen;
    private int cantidad;

    public existencia() {
    }

    public existencia(int exist, almacen almacen, int cantidad) {
        this.exist = exist;
        this.almacen = almacen;
        this.cantidad = cantidad;
    }

    public int getExist() {
        return exist;
    }

    public void setExist(int exist) {
        this.exist = exist;
    }

    public almacen getAlmacen() {
        return almacen;
    }

    public void setAlmacen(almacen almacen) {
        this.almacen = almacen;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    @Override
    public String toString() {
        return "existencia{" + "exist=" + exist + ", almacen=" + almacen + ", cantidad=" + cantidad + '}';
    }
    public void imprimir(){
    System.out.println(toString());
    
}

    
}
