
package inv;
public  class articulo {
private String  nombre;
private int clave;

    public articulo() {
        
    }

    public articulo(String nombre, int clave) {
        this.nombre = nombre;
        this.clave = clave;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getClave() {
        return clave;
    }

    public void setClave(int clave) {
        this.clave = clave;
    }

    @Override
    public String toString() {
        return "articulo{" + "nombre=" + nombre + ", clave=" + clave + '}';
    }
public void imprimir(){
    System.out.println(toString());
    
}
}