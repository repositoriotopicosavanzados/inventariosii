
package inv;

public  class almacen {
    
    private String nombreA;
    private int claveA;

    public almacen() {
    }

    public almacen(String nombreA, int claveA) {
        this.nombreA = nombreA;
        this.claveA = claveA;
    }

    public String getNombreA() {
        return nombreA;
    }

    public void setNombreA(String nombreA) {
        this.nombreA = nombreA;
    }

    public int getClaveA() {
        return claveA;
    }

    public void setClaveA(int claveA) {
        this.claveA = claveA;
    }

    @Override
    public String toString() {
        return "almacen{" + "nombreA=" + nombreA + ", claveA=" + claveA + '}';
    }
   public void imprimir(){
    System.out.println(toString());
    
}
    
}
